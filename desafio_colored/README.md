## Para buildar:
```
  docker build -t mandaveh/my-colored .
```

## Para executar:
```
  docker run -d --name my-colored -p 8087:9090 -e APP_NAME=<seu nome> mandaveh/my-colored
```

## Para ver o resultado:
- Acessar o endereço localhost:8087 no seu browser preferido

## Dica:

 **Para escolher outra cor de fundo da pagina, basta passar a variavel de ambiente APP_COLOR informando um nome de cor em inglês.**